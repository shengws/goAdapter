package setting

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	//"strings"
)
/*
type SnmpNameListTemplate struct {
	Name []string `json:"Name"`
}*/

type SnmpParamTemplate struct{
	DeviceName string
	DeviceType string
	DeviceBrand string
	DeviceModel string
	IpAddress string
	Port int
	ReadCommunity string
	WriteCommunity string
	Timeout int
	Frequency int
	Retry int
	SNMPVersion string
}

type SnmpParamListTemplate struct{
	SnmpParam []*SnmpParamTemplate
}

//var SnmpNameList  = SnmpNameListTemplate{}
var SnmpParamList = &SnmpParamListTemplate{
	SnmpParam: make([]*SnmpParamTemplate,0),
}

func init(){

}

func SnmpWalk(host string, oid string)(s []string) {
	command := "snmpwalk -v 2c -c public " + host + " " + oid
	cmd := &exec.Cmd{}
	switch runtime.GOOS {
		case "windows":
			cmd = exec.Command("cmd", "/c", command)
		case "linux":
			cmd = exec.Command("/bin/sh", "-c", command)
	}
	output,err := cmd.Output()
	str := string(output)
	splitString := strings.Split(str, "\r\n")
	fmt.Println(splitString, len(splitString))
	if err != nil {
		log.Fatal(err)
	}
	return splitString
}

func (n *SnmpParamListTemplate)CreatSnmpParam(name string){

	snmpParam := &SnmpParamTemplate{
		DeviceName: name,
		DeviceType: "type",
		DeviceBrand: "string",
		DeviceModel: "string",
		IpAddress: "string",
		Port: 161,
		ReadCommunity: "string",
		WriteCommunity: "string",
		Timeout: 1000,
		Frequency: 60000,
		Retry: 1,
		SNMPVersion: "2",
	}

	n.SnmpParam = append(n.SnmpParam,snmpParam)
}

/*
func (n *SnmpParamListTemplate)GetSnmpParam(){

	for _,v := range n.SnmpParam{

		snmpInfo,err := GetSnmpInformation(v.DeviceName)

		if err == nil{
			v.IP = snmpInfo.
			v.Netmask = snmpInfo.Mask
			v.Broadcast = snmpInfo.GatewayIP
			v.MAC = strings.ToUpper(snmpInfo.Mac)
		}

		//v.GetSnmpStatus()
	}
}
*/

func (n *SnmpParamListTemplate)SetSnmpParam(param SnmpParamTemplate) {

	for _,v := range n.SnmpParam{
		if v.DeviceName == param.DeviceName{
			v.DeviceType = param.DeviceType
			v.DeviceBrand = param.DeviceBrand
			v.DeviceModel = param.DeviceModel
			v.IpAddress = param.IpAddress
			v.Port = param.Port
		}
	}

	SnmpParamWrite()
}

func SnmpParamRead() bool {

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	fileDir := exeCurDir + "/selfpara/snmpparam.json"

	if fileExist(fileDir) == true {
		fp, err := os.OpenFile(fileDir, os.O_RDONLY, 0777)
		if err != nil {
			fmt.Println("open snmpparam.json err", err)
			return false
		}
		defer fp.Close()

		data := make([]byte, 500)
		dataCnt, err := fp.Read(data)

		//fmt.Println(string(data[:dataCnt]))

		err = json.Unmarshal(data[:dataCnt], &SnmpParamList)
		if err != nil {
			fmt.Println("snmpparam unmarshal err", err)

			return false
		}
/*
		for _,v := range SnmpParamList.SnmpParam{
			if v.DHCP == "1"{
				v.CmdSetDHCP()
			}else if v.DHCP == "0"{
				v.CmdSetStaticIP()
			}
		}*/

		return true
	} else {
		fmt.Println("snmpparam.json is not exist")

		os.MkdirAll(exeCurDir+"/selfpara", os.ModePerm)
		fp, err := os.Create(fileDir)
		if err != nil {
			fmt.Println("create snmpparam.json err", err)
			return false
		}
		defer fp.Close()
/*
		log.Printf("snmpName %v\n",SnmpNameList)
		for _,v := range SnmpNameList.Name{
			SnmpParamList.CreatSnmpParam(v)
			SnmpParamWrite()
		}
*/
		return true
	}
}

func SnmpParamWrite() {

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	fileDir := exeCurDir + "/selfpara/snmpparam.json"

	fp, err := os.OpenFile(fileDir, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		fmt.Println("open snmpparam.json err", err)
	}
	defer fp.Close()

	sJson, _ := json.Marshal(SnmpParamList)
	fmt.Println(string(sJson))

	_, err = fp.Write(sJson)
	if err != nil {
		fmt.Println("write snmpparam.json err", err)
	}
	fmt.Println("write snmpparam.json ok")
	fp.Sync()
}
/*
func (n *NetworkParamTemplate)GetNetworkStatus(){

	ethHandle, _ := ethtool.NewEthtool()
	defer ethHandle.Close()

	n.LinkStatus, _ = ethHandle.LinkState(n.Name)
}
*/
/*
func (n *NetworkParamTemplate)CmdSetDHCP(){

	//cmd := exec.Command("udhcpc","-i",n.Name,"5")
	cmd := exec.Command("udhcpc","-i",n.Name)

	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Run()	//执行到此会阻塞5s

	str := out.String()

	log.Println(str)
}

func (n *NetworkParamTemplate)CmdSetStaticIP(){

	strNetMask   := "netmask " + n.Netmask
	cmd := exec.Command("ifconfig",
		n.Name,
		n.IP,
		strNetMask)

	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Start()	//执行到此,直接往后执行

	cmd2 := exec.Command("/sbin/route","add","default","gw",n.Broadcast)
	cmd2.Stdout = &out
	cmd2.Start()	//执行到此,直接往后执行
}

func findNetCard(name string) (string, error) {
	if runtime.GOOS == "linux" {
		inters, err := net.Interfaces()
		if err != nil {
			return "", err
		}
		for _, v := range inters {
			if v.Name == name {
				return name, nil
			}
		}
	}
	return "", fmt.Errorf("not support GOOS(%s) and GOARCH(%s)",
		runtime.GOOS, runtime.GOARCH)
}

// HardwareAddr get mac address, if failed,it is empty
func HardwareAddr(name string) (net.HardwareAddr, error) {
	netCard, err := findNetCard(name)
	if err != nil {
		return net.HardwareAddr{}, err
	}
	inter, err := net.InterfaceByName(netCard)
	if err != nil {
		return net.HardwareAddr{}, err
	}
	return inter.HardwareAddr, err
}

// 通过网卡获得 MAC IP IPMask GatewayIP
func GetNetInformation(netName string) (NetInformation, error) {
	info := NetInformation{}

	card, err := findNetCard(netName)
	if err != nil {
		return info, err
	}
	info.InterName = card

	inter, err := net.InterfaceByName(card)
	if err != nil {
		return info, err
	}

	info.HardwareAddr = inter.HardwareAddr
	info.Mac = hex.EncodeToString(inter.HardwareAddr)

	address, err := inter.Addrs()
	if err != nil {
		return info, err
	}
	for _, addr := range address {
		if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				info.IP = ipnet.IP.String()
				info.Mask = net.IP(ipnet.Mask).String()
			}
		}
	}
	//获取网关Ip
	out, err := exec.Command("/bin/sh", "-c",
		fmt.Sprintf("route -n | grep %s | grep UG | awk '{print $2}'", card)).Output()
	if err != nil {
		return info, err
	}
	info.GatewayIP = strings.Trim(string(out), "\n")
	return info, nil
}

func fileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}
*/
